package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Employee;

@Service
public class EmployeeDAO {
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	public List<Employee> getEmployees() {
		return employeeRepository.findAll();
	}
	
	public Employee getEmployeeById(int empId) {
		return employeeRepository.findById(empId).orElse(new Employee(0, "Not Found", 0.0, "", "", "", "", null));
	}

	public Employee getEmployeeByName(String empName) {
		return employeeRepository.findByName(empName);
	}
	
	public void registerEmployee(Employee emp) {
		BCryptPasswordEncoder  enp = new BCryptPasswordEncoder() ;
		String bcryptpw = enp.encode(emp.getPassword());
		emp.setPassword(bcryptpw);
		employeeRepository.save(emp);
	}
	
	public void updateEmployee(Employee emp) {
		employeeRepository.save(emp);
	}
	
	public void deleteEmployeeById(int empId) {
		employeeRepository.deleteById(empId);
	}	
	public Employee empLogin(String emailId, String password) {
		  return employeeRepository.empLogin(emailId, password);
		}

	public Employee getEmployeByEmailId(String emailId) {
		return employeeRepository.findByEmail(emailId);
	}
}