import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { ReimbursementComponent } from './reimbursement/reimbursement.component';
import { AuthGuard } from './auth.guard';
import { TaxesComponent } from './taxes/taxes.component';

const routes: Routes = [
  {path : "" , component: HomeComponent},
  {path : "login", component:LoginComponent },
  {path : "register", component:RegisterComponent},
  {path : "logout", component:LogoutComponent},
  {path : "reimbursement", canActivate: [AuthGuard] ,component:ReimbursementComponent},
  {path : "taxes", canActivate:[AuthGuard], component:TaxesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
