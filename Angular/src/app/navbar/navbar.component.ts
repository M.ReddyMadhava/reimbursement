// navbar.component.ts
import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {

  isUserLogin : boolean ;
  constructor(private service : EmpService) {
    this.isUserLogin = service.isUserLogged;
  }

  

  
}
