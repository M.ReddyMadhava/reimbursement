import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  template: '', // You can have an empty template or a message if needed.
})
export class LogoutComponent {
  constructor(private router: Router) {
    // Perform logout logic here, such as clearing session data.
    // Then redirect the user to the login page.
    this.logout();
  }

  logout() {
    // Clear session data, JWT tokens, etc.
    // For example, you might use a service or storage mechanism to do this.
    // Then redirect the user to the login page.
    this.router.navigate(['/login']);
  }
}
