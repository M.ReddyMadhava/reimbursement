import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  
  employees: any;
  employee: any;

  //Dependency Injection for Router, EmpService
  constructor(private router: Router, private service: EmpService, private toster : ToastrService) {

    //Delete All the Employee Hardcoded JSON objects

  }

  async validateLogin(loginForm: any) {
    console.log(loginForm);

    //Implementing LocalStorage
    localStorage.setItem("emailId", loginForm.emailId);

    if (loginForm.emailId == "HR" && loginForm.password == "HR") {
      this.service.setUserLoggedIn();
      this.router.navigate(['home']);
      this.toster.success("Login Success !");
    } else {

      await this.service.empLogin(loginForm).then((data: any) => {
        console.log(data);
        this.employee = data;
      });

      if (this.employee != null) {
        this.service.setUserLoggedIn();
        this.router.navigate(['home']);
        this.toster.success("Login Success !");
      } else {
       this.toster.error("Invalid Credentials"); 
      }
    }
  }
}
