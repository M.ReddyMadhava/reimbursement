import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  isUserLogged: boolean;
  loginStatus: any;

  //Dependency Injection for HTTPClient
  constructor(private http: HttpClient) {
    this.isUserLogged = false;
    this.loginStatus = new Subject();
  }

  getCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }
  getEmployees(): any {
    return this.http.get('http://localhost:8085/getEmployees');
  }
  getEmpById(empId: any): any {
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
  }
  employeeRegister(employee: any) {
    return this.http.post('registerEmployee', employee);
  }
  getDepartments(): any {
    return this.http.get('getDepartments');
  }
  deleteEmployee(empId: any) {
    return this.http.delete('deleteEmployeeById/' + empId);
  }
  updateEmployee(employee: any) {
    return this.http.put('updateEmployee', employee);
  }
  empLogin(employee: any) {
    return this.http.get('empLogin/' + employee.emailId + "/" + employee.password).toPromise();
  }




  //Successfully Logged In
  setUserLoggedIn() {
    this.isUserLogged = true;
  }

   //Logout
  setUserLoggedOut() {
    this.isUserLogged = false;
  }

  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }



}
